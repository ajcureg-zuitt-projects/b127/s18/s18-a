// 1) Create a movies array
// 2) Add 5 movies objects with the fllowing properties:

// 	titlekey: 		//string
// 	genre: 			//string
// 	releasedDate: 	//date
// 	rating: 		//number
// 3) Add a method called displayRating (console.log)

// 	output: The movie ____ has ___ stars.
// 4) Create a function called showAllMovies(arr) that displays all the movie titles and genre)

// 	Output:
// 	Godfather is an action movie
// 	Avengers is an action movie
// 	Exorcist is a horror movie

// movies[0].title

let movies = [
		movie1 = {
			title: 'Tangled',
			genre: 'Animation',
			releasedDAte: d= new Date (2010, 10, 14), // November 14, 2010
			rating: 4.8,
			displayRating: function(){
				return 'The movie "' + this.title + '" has ' + this.rating + ' stars.';
			},
			// console.log('The movie "' + this.title + '" has ' + this.rating + ' stars.')
			// },

			displayGenre: function(){
			return 'The movie "' + this.title + '" is an ' + this.genre + ' movie.';
			}
		},

		movie2 = {
			title: 'The Notebook',
			genre: 'Romance/ Drama',
			releasedDAte:  d= new Date (2004, 4, 20), // May 20, 2004
			rating: 4.7,
			displayRating: function(){
			return 'The movie "' + this.title + '" has ' + this.rating + ' stars.';
			},
			displayGenre: function(){
			return 'The movie "' + this.title + '" is a ' + this.genre + ' movie.';
			}
		},
	
		movie3 = {
			title: 'Parasite',
			genre: 'Thriller/ Comedy',
			releasedDAte:  d= new Date (2019, 4, 21), // 21 May 2019
			rating: 4.6,
			displayRating: function(){
			return 'The movie "' + this.title + '" has ' + this.rating + ' stars.';
			},
			displayGenre: function(){
			return 'The movie "' + this.title + '" is a ' + this.genre + ' movie.';
			}
		},

		movie4 = {
			title: 'The Dictator',
			genre: 'Comedy',
			releasedDAte: d= new Date (2012, 4, 16), // May 16, 2012
			rating: 4.6,
			displayRating: function(){
			return 'The movie "' + this.title + '" has ' + this.rating + ' stars.';
			},
			displayGenre: function(){
			return 'The movie "' + this.title + '" is a ' + this.genre + ' movie.';
			}
		},

		movie4 = {
			title: 'The Greatest Showman',
			genre: 'Musical/ Drama',
			releasedDAte: d= new Date (2017, 11, 8), // December 8, 2017
			rating: 4.8,
			displayRating: function(){
			return 'The movie "' + this.title + '" has ' + this.rating + ' stars.';
			},
			displayGenre: function(){
			return 'The movie "' + this.title + '" is a ' + this.genre + ' movie.';
			}
		}
]


// Method
console.log(movies[0].displayRating());
console.log(movies[1].displayRating());
console.log(movies[2].displayRating());
console.log(movies[3].displayRating());
console.log(movies[4].displayRating());

// Function

// let 
// function showallmovies(movies){
// 	for (let i = 0; i < movies.length; i++);
// 		for (let j = 0; j < movies[i]. length; j++){
// console.log('The movie "' + this.title + '" has ' + this.genre + ' movie.')
// 	}
// }

function showallmovies(movies){
for (let i = 0 ; i<movies.length; i++){
 movies[i].displayGenre()
 }
}

